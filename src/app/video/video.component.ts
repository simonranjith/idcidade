import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { NotesService } from '../notes-service.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css', '../app.component.css']
})
export class VideoComponent implements OnInit {
  @Output() noteCreated = new EventEmitter<any>();

  public notes: Array<any>;
  displayMenubar : boolean;
  statusVisible : boolean;
  infoDialogVisible : boolean;
  dialogTitle : string;
  public currentNote: any;
  Error: string;

  constructor(private notesService: NotesService) {
    this.displayMenubar = true;
    this.Error = "";
    notesService.get().subscribe((data: any) => this.notes = data);
  }

  ngOnInit(): void {
  }

  public editNote = function(record: any) {
    this.statusVisible = true;
    this.dialogTitle = "Edit Video";
    this.currentNote = record;
  };

  public newNote = function () {
    this.statusVisible = true;
    this.dialogTitle = "Add Video";
    this.currentNote = this.getDefaultNote();
  };

  private getDefaultNote() {
    return {
      id: undefined,
      title: '',
      content: '',
      live: ''
    }
  }

  OnEditCancel() {
    this.statusVisible = false;
  }

  OnEditSubmit() {
    this.createUpdateNote(this.currentNote);
    this.statusVisible = false;
  }

  public createUpdateNote = function (note: any) {
    let noteWithId = _.find(this.notes, (el => el.id === note.id));
    if (noteWithId) {
      const updateIndex = _.findIndex(this.notes, { id: noteWithId.id });
      this.notesService.updateVideo(note).subscribe(
        data => {
          if (data.value == 1) {
              this.notes.splice(updateIndex, 1, note);
              this.statusVisible = false;
            return;
        }
        
        this.Error = "Message: Unable to update video";
        
      });
    } else {
      this.notesService.addVideo(note).subscribe(
        noteRecord => {
        
        if (noteRecord.value > 0) {
          note.id = noteRecord.id;
          //this.notes.push(note)
          this.notes = [...this.notes, note];
          this.statusVisible = false;
          return;
        }
          this.Error = "Message: Unable to add video";
        }
      );
    }
    this.currentNote = this.getDefaultNote();
    //this.statusVisible = false;
  };

  public deleteNote(record) {
    this.infoDialogVisible = true;
    this.Error = "Processing ... ";
    const deleteIndex = _.findIndex(this.notes, { id: record.id });
    this.notesService.deleteVideo(record).subscribe(
      data => {
        if (data.value == 1) {
            this.notes.splice(deleteIndex, 1);
            this.infoDialogVisible = false;
            return;
        }
        this.Error = "Message: Unable to delete video";
      //result => this.notes.splice(deleteIndex, 1)
      });
  }
}
