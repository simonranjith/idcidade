import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-update-notes',
  templateUrl: './edit-video.component.html',
  styleUrls: ['./edit-video.component.css']
})
export class EditVideoComponent implements OnInit {
  //@Output() noteCreated = new EventEmitter<any>();
  @Input() note: any;
  constructor() {
    this.clearNotes();
   }

  ngOnInit(): void {
    this.clearNotes();
  }
  // Create an empty note object
  private clearNotes = function () {
    this.note = {
      id: undefined,
      title: '',
      content: '',
      live: 0
    };
  };
  public addUpdateNote = function(event) {
    //this.noteCreated.emit(this.note);
    //this.clearNotes();
  };
}
