import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { NotesService } from '../notes-service.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit {

  displayMenubar : boolean;
  statusVisible : boolean;
  dialogTitle : string;
  Error: string;

  public live: Array<any>;
  public currentLive: any;

  constructor(private notesService: NotesService) {
    this.displayMenubar = true;
    this.Error = "";
    notesService.getLive().subscribe((data: any) => {this.live = data});
  }

  public editLive = function(record: any) {
    this.statusVisible = true;
    this.dialogTitle = "Edit Live";
    this.currentLive = record;
  };

  OnEditCancel() {
    this.statusVisible = false;
  }

  OnEditSubmit() {
    this.updateLive(this.currentLive);
  }

  public updateLive = function(video:any){
    let videoWithId = _.find(this.live, (el => el.id === video.id));
    if (videoWithId) {
      const updateIndex = _.findIndex(this.notes, { id: videoWithId.id });
      this.notesService.updateVideo(video).subscribe(
        data => {
          if (data.value == 1) {
          this.live.splice(updateIndex, 1, video);
          this.statusVisible = false;
            return;
        }
        
        this.Error = "Message: Unable to update live video";
      });
    }
  }

  ngOnInit(): void {
  }

}
