import { Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import { DialogComponent } from '../dialog/dialog.component';
//import {Column} from 'primeng/primeng';
//import {Paginator} from 'primeng/primeng';

@Component({
  selector: 'app-notes-listing',
  templateUrl: './video-listing.component.html',
  styleUrls: ['./video-listing.component.css'],
})
export class VideoListingComponent implements OnInit {

  @Input() notes: Array<any>;
  @Output() recordDeleted = new EventEmitter<any>();
  @Output() newClicked = new EventEmitter<any>();
  @Output() editClicked = new EventEmitter<any>();
  public delete(data) {
    this.recordDeleted.emit(data);
  }
  public edit(data) {
    this.editClicked.emit(Object.assign({}, data));
  }
  
  public new() {
    this.newClicked.emit();
  }
  
  ngOnInit(): void {
  }
  /*ngAfterViewInit() {
  }*/
}
