import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.css'],
  inputs: ['visible']
})
export class MenubarComponent implements OnInit {

  visible: boolean;
  constructor() { 
    this.visible = false;
  }

  ngOnInit(): void {
  }

}
