import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-live',
  templateUrl: './edit-live.component.html',
  styleUrls: ['./edit-live.component.css']
})
export class EditLiveComponent implements OnInit {

  @Input() live: any;

  constructor() { }

  ngOnInit(): void {
    this.clearLive();
  }

  // Create an empty live object
  private clearLive = function () {
    this.live = {
      id: undefined,
      title: '',
      content: '',
      live: true
    };
  };

}
