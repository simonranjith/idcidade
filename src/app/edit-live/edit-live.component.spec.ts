import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLiveComponent } from './edit-live.component';

describe('EditLiveComponent', () => {
  let component: EditLiveComponent;
  let fixture: ComponentFixture<EditLiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditLiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
