import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers, Http,URLSearchParams } from '@angular/http'
import {map} from 'rxjs/operators';

@Injectable(/*{
  providedIn: 'root'
}*/)
export class NotesService {
  private headers: Headers;
  private api: string = 'https://localhost:5001/api/'
  private videos = this.api + 'videos/';
  private msgs = this.api + 'messages/';
  private users = this.api + 'users/';

  private getAllVideosURL : string = this.videos + 'getvideos';
  private deleteVideoURL : string = this.videos + 'deletevideo';
  private updateVideoURL : string = this.videos + 'updatevideo';
  private addVideoURL : string = this.videos + 'addvideo';

  private getAllMsgsURL : string = this.msgs + 'getmsgs';
  private deleteMsgURL : string = this.msgs + 'deletemsg';

  private getAllUsersURL : string = this.users + 'getusers';
  private updateUserURL : string = this.users + 'updateuser';

  private getLiveVideoURL : string = this.videos + 'getlive';

  private loginURL : string = this.users + "login";

  constructor(private http: Http) { 
    this.headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
  }
  // Get all videos
  public get() {
    return (<any>this.http.get(this.getAllVideosURL, {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  // Get all msgs
  public getMsgs() {
    return (<any>this.http.get(this.getAllMsgsURL, {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  // Get all users 
  public getUsers() {
    return (<any>this.http.get(this.getAllUsersURL , {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  // Get live video
  public getLive() {
    return (<any>this.http.get(this.getLiveVideoURL, {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  public add(payload) {
    return this.http.post(this.getAllVideosURL, payload, { headers: this.headers });
  }
  public remove(payload) {
    return this.http.delete(this.getAllVideosURL + '/' + payload.id, { headers: this.headers });
  }
  public update(payload) {
    return this.http.put(this.getAllVideosURL + '/' + payload.id, payload, { headers: this.headers });
  }

  public validateLogin(name: string, pwd: string) {
    var params = new URLSearchParams();
          params.set('username', name);
          params.set('password', pwd);

    return (<any>this.http.post(this.loginURL, params.toString(), {
        headers: this.headers
    }))
    .pipe(map((res: Response) => {
      return res.json();
    }));
  }

  public updateUser(payload) {

    var params = new URLSearchParams();
          params.set('id', payload.id);
          params.set('username', payload.name);
          params.set('email', payload.email);
          params.set('password', payload.password);

    return (<any>this.http.post(this.updateUserURL, params.toString(), 
    { headers: this.headers }))
    .pipe(map((res: Response) => {
      alert(res.json());
      return res.json();
    }));
  }

  public addVideo(payload) {

    var params = new URLSearchParams();
          params.set('id', payload.id);
          params.set('title', payload.title);
          params.set('content', payload.content);
          params.set('live', payload.live);

    return (<any>this.http.post(this.addVideoURL, params.toString(), 
    { headers: this.headers }))
    .pipe(map((res: Response) => {
      alert(res.json());
      return res.json();
    }));
  }

  public updateVideo(payload) {

    var params = new URLSearchParams();
          params.set('id', payload.id);
          params.set('title', payload.title);
          params.set('content', payload.content);
          params.set('live', payload.live);

    return (<any>this.http.post(this.updateVideoURL, params.toString(), 
    { headers: this.headers }))
    .pipe(map((res: Response) => {
      alert(res.json());
      return res.json();
    }));
  }

  public deleteVideo(payload) {

    var params = new URLSearchParams();
          params.set('id', payload.id);

    return (<any>this.http.post(this.deleteVideoURL, params.toString(), 
    { headers: this.headers }))
    .pipe(map((res: Response) => {
      alert(res.json());
      return res.json();
    }));
  }

  public deleteMsg(payload) {

    var params = new URLSearchParams();
          params.set('id', payload.id);

    return (<any>this.http.post(this.deleteMsgURL, params.toString(), 
    { headers: this.headers }))
    .pipe(map((res: Response) => {
      alert(res.json());
      return res.json();
    }));
  }
}
