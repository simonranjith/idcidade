import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { NotesService } from '../notes-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../app.component.css']
})
export class HomeComponent implements OnInit {

  displayMenubar : boolean;
  statusVisible : boolean;
  dialogTitle : string;

  public users: Array<any>;
  public currentUser: any;
  Error: string;

  constructor(private notesService: NotesService) {
    this.displayMenubar = true;
    this.Error = "";
    notesService.getUsers().subscribe((data: any) => this.users = data);
   }

   public editUser = function(record: any) {

    var eleName = document.getElementById("usr-password");
    record.password = eleName.nodeValue;

    this.statusVisible = true;
    this.dialogTitle = "Edit User";
    this.currentUser = record;
  };

  OnEditCancel() {
    this.statusVisible = false;
  }

  OnEditSubmit() {
    this.updateUser(this.currentUser);
  }

  public updateUser = function(user:any){
    let userWithId = _.find(this.users, (el => el.id === user.id));
    if (userWithId) {
      const updateIndex = _.findIndex(this.users, { id: userWithId.id });
      
      this.notesService.updateUser(user).subscribe(
        data => {
        if (data.value == 1) {
            this.users.splice(updateIndex, 1, user);
            this.statusVisible = false;
            return;
        }
        
        this.Error = "Message: Unable to update user";
        
      }
      );
    }
  }

  ngOnInit(): void {
  }

}
