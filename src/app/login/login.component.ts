import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotesService } from '../notes-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  displayMenubar: boolean;
  Error: string;

  constructor(private router: Router, private notesService: NotesService) { 
    this.displayMenubar = false;
    this.Error = "";
  }

  ngOnInit(): void {
  }

  enteredUsername() {
    if (this.password != "") this.login();
    else {
        var elePwd = document.getElementById("pwd");
        elePwd.focus();
    }
        
}

  login()
  {
    this.Error = "Processing ... ";
    this.notesService.validateLogin(this.username, this.password).subscribe(
      data => {
        //alert(data['id']); 
          if(data['id'] > 0)
          {
            this.Error = "";
            //alert('Login validated'+ data['id']);
            this.router.navigate(['/home']);
          }

          this.Error = "Message: Invalid credentials";
      });
  }
}
