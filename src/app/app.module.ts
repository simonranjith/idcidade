import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {TableModule} from 'primeng/table';

import { DialogComponent } from './dialog/dialog.component'
import { VideoListingComponent } from './video-listing/video-listing.component';
import { EditVideoComponent } from './edit-video/edit-video.component';
import { NotesService } from './notes-service.service';
import { VideoComponent } from './video/video.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { MenubarComponent } from './menubar/menubar.component';
import { MessageComponent } from './message/message.component';
import { HomeComponent } from './home/home.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { LiveComponent } from './live/live.component';
import { EditLiveComponent } from './edit-live/edit-live.component';

const appRoutes: Routes = [
  //{ path: '', component: AppComponent },
  {path: "", redirectTo: "login", pathMatch: "full"},
  { path: 'video', component: VideoComponent },
  { path: 'message', component: MessageComponent },
  { path: 'home', component: HomeComponent },
  { path: 'live', component: LiveComponent },
  //{ path: 'second-component', component: EditVideoComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', component: PageNotFoundComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    VideoListingComponent,
    EditVideoComponent,
    VideoComponent,
    PageNotFoundComponent,
    DialogComponent,
    LoginComponent,
    MenubarComponent,
    MessageComponent,
    HomeComponent,
    EditUserComponent,
    LiveComponent,
    EditLiveComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    AppRoutingModule,
		FormsModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    TableModule 
  ],
  providers: [NotesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
