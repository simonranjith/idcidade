import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { NotesService } from '../notes-service.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  public msgs: Array<any>;
  Error: string;
  infoDialogVisible : boolean;

  constructor(private notesService: NotesService) { 
    this.displayMenubar = true;
    this.Error = "";
    notesService.getMsgs().subscribe((data: any) => this.msgs = data);
  }
  displayMenubar : boolean;
  ngOnInit(): void {
  }

  public deleteMsg(record) {
    this.infoDialogVisible = true;
    this.Error = "Processing ... ";
    const deleteIndex = _.findIndex(this.msgs, { id: record.id });
    this.notesService.deleteMsg(record).subscribe(
      data => {
        if (data.value == 1) {
            this.msgs.splice(deleteIndex, 1);
            this.infoDialogVisible = false;
            return;
        }
        this.Error = "Message: Unable to delete video";
      //result => this.notes.splice(deleteIndex, 1)
      });
  }

}
