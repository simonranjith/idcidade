import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditVideoComponent } from './edit-video/edit-video.component';
import { VideoListingComponent } from './video-listing/video-listing.component';
import { VideoComponent } from './video/video.component';

const routes: Routes = [
  //{ path: 'second-component', component: NotesListingComponent }//,
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
